<?php
declare(strict_types=1);

/* On inclu la confioguration */
require('config/config.php');


/* Variable générique pour le layout */
const LAYOUT_VIEW = 'order/orders';
const LAYOUT_TITLE = 'Liste des commande';



try {
    /* 1. Connecter au SGBD Mysql */
    $dbh = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
    /* On dit à PDO de lever des exceptions plutôt que des erreurs */
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /* 2. Préparer une requête SQL */
    $sth = $dbh->prepare('SELECT * FROM orders');

    /* 3. Lier des donner et l'executer */
    $sth->execute();

    /* 4. Récupérer le jeu d'enregistrement */
    $orders = $sth->fetchAll(PDO::FETCH_ASSOC);
    
    /** Inclu le layout */
    require('tpl/layout.phtml');
}
catch (PDOException $e) {
    echo 'Navré notre serveur de BDD est down !';
    echo $e->getMessage();
}

